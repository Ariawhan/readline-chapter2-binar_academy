// Soal
// 1. buat function penjumlahan dengan parameter (operator, x, y, z)
// 2. gunakan user input via terminal dengan readline module untuk nilai (operator, x, y, z)
// 3. didalam function berikan 4 kondisi :
// - jika nilai operator = perkalian, maka lakukan perkalian x * y * z return hasilnya
// - jika nilai operator = pembagian, maka lakukan pembagian x / y / z return hasilnya
// - jika nilai operator = campuran, maka lakukan penjumlahan x * y / z return hasilnya
// - selain itu semua, default kondisinya, lakukan penjumlahan x - y + z return hasilnya
// 4. console.log atau print = "ini hasil dari penjumlahan x y z = ", penjumlahan(operator, x, y, z)

//Implemtasi code
// Library readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

//Fungsi Penjumlahan
function Arithmetic(operator, x, y, z) {
  if (operator == 1) {
    return x * y * z;
  } else if (operator == 2) {
    return x / y / z;
  } else {
    return (x * y) / z;
  }
}

//Tampilan Awal
console.log(
  "Aplikasi Perhitungan\n 1. Perkalian\n 2. Pembagian\n 3. campuran\n"
);

//Pemanggilan readline
rl.question("Jawab : ", (answerOperator) => [
  rl.question("Masukan x : ", (answerX) => [
    rl.question("Masukan y : ", (answerY) => [
      rl.question("Masukan z : ", (answerZ) => [
        console.log(
          "\nHasil Perhitungan : " +
            Arithmetic(
              parseInt(answerOperator),
              parseFloat(answerX),
              parseFloat(answerY),
              parseFloat(answerZ)
            )
        ),
        rl.close(),
      ]),
    ]),
  ]),
]);
